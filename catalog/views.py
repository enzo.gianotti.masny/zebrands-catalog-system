from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import UserSerializer, BrandSerializer, ProductSerializer, ProductCounterSerializer
from .models import Brand, Product, ProductCounter


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited by admin users.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

    class Meta:
        extra_kwargs = {'password': {'write_only': True}}


class BrandViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows brands to be viewed or edited by admin users.
    """
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows products to be created or edited by admins and only viewed by anonymous users.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def update_product_count(self, product_id):
        """
        Increases the counter of a product by one

        :param product_id: int
            product primary key

        :return: ProductCounter
            returns updated ProductCounter instance
        """

        product = Product.objects.get(pk=product_id)
        product_count, created = ProductCounter.objects.get_or_create(product=product)
        product_count.counter += 1
        product_count.save()

        return product_count

    def retrieve(self, request, pk, *args, **kwargs):
        """
        Keep track of the number of times every single product is queried by an anonymous user
        """
        if request.user.is_anonymous:
            self.update_product_count(pk)

        return super().retrieve(request, *args, **kwargs)


class ProductCountViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows count viewed products by anonymous users.
    """
    queryset = ProductCounter.objects.all()
    serializer_class = ProductCounterSerializer
    permission_classes = [permissions.IsAuthenticated]
