from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Brand, Product
from requests.auth import HTTPBasicAuth
from django.contrib.auth.models import User
from catalog.notifications import AWSSESProductNotification


class BrandTests(APITestCase):

    def setUp(self):
        """
        Create admin user
        """
        self.user = User.objects.create_user(username='admin', password='admin', email='enzo.gianotti.masny@gmail.com')

    def test_check_admin_user(self):
        """
        Ensure we can create a new admin user.
        """
        self.client.force_authenticate(self.user)
        self.assertEqual(self.user.username, 'admin')

    def test_permission_anoynmous_user_brand(self):
        """
        Check permissions on anoymous user
        """
        url = reverse('brand-list')
        data = {'name': 'Lunna'}
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.delete(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    def test_admin_create_brand(self):
        """
        Ensure we can create a new brand object.
        """

        self.client.force_authenticate(self.user)
        url = reverse('brand-list')
        data = {'name': 'Lunna'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Brand.objects.count(), 1)
        self.assertEqual(Brand.objects.get().name, 'Lunna')



class NotificationTests(APITestCase):

    def setUp(self):
        """
        Create admin user
        """
        self.user = User.objects.create_user(username='admin', password='admin', email='enzo.gianotti.masny@gmail.com')


    def test_send_notification_failed(self):
        """
        Check send notification failed
        """
        brand = Brand.objects.create(name="Lunna")
        product = Product.objects.create(sku="123-123", name="New Product", price=1000.00, brand=brand)

        notification = AWSSESProductNotification()
        response = notification.send(product)

        self.assertEqual(response['success'], False)




