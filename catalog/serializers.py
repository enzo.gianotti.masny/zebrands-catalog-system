from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Brand, Product, ProductCounter
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from .notifications import AWSSESProductNotification


class UserSerializer(serializers.HyperlinkedModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'password', 'password2']

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return instance


class BrandSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Brand
        fields = ['url', 'id', 'name']


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    brand = serializers.SlugRelatedField(
        read_only=False,
        slug_field='name',
        queryset=Brand.objects.all()
    )

    class Meta:
        model = Product
        fields = ['url', 'id', 'sku', 'name', 'price', 'brand']

    def update(self, instance, validated_data):
        # updates product
        instance = super(ProductSerializer, self).update(instance, validated_data)

        # adds extra attributes for email body text
        url = self['url'].value
        instance.url = url
        instance.updated_by = self.context['request'].user

        # notifies  to admin users a change in a product
        notification = AWSSESProductNotification()
        notification.send(instance)

        return instance


class ProductCounterSerializer(serializers.ModelSerializer):
    product_name = serializers.CharField(source='product.name')

    class Meta:
        model = ProductCounter
        fields = ['product', 'product_name', 'counter']
