from rest_framework import routers
from catalog import views

router = routers.DefaultRouter()
router.register(r'api/users', views.UserViewSet)
router.register(r'api/catalog/brands', views.BrandViewSet)
router.register(r'api/catalog/products', views.ProductViewSet)
router.register(r'api/statistics/products/count', views.ProductCountViewSet)
