import boto3
from botocore.exceptions import ClientError
from abc import ABC, abstractmethod
from django.contrib.auth.models import User
from dotenv import dotenv_values
import os


class Notification(ABC):
    """
    Abstract class to implement differents type of notifications
    """

    @abstractmethod
    def send(self):
        """
        Force to override the implementation in subclasses
        """
        pass


class AWSSESProductNotification(Notification):
    """
    Notifications via AWS Simple Email Service
    """

    def get_email_from_users(self):
        """
        get emails from model User

        :return: list
            user emails list
        """

        return list(User.objects.all().values_list('email', flat=True))

    def send(self, product):
        """
        Notifies all admin users of a change in a product via email
        If .env file is not present, the notification will not sended

        :param product: Product
            product instance

        :return: dict
            structure:
                {
                    'success': boolean,
                    'message': str
                }
        """

        try:
            # check if .env file exist
            if not os.path.exists(".env"):
                raise Exception("You need to configure .env file with AWS Credentials to send notification emails")
            else:
                # set AWS SES parameters
                sender = "enzo.gianotti.masny@gmail.com"
                recipients = self.get_email_from_users()
                aws_region = "sa-east-1"
                charset = "UTF-8"
                subject = f"The product {product} has been changed"
                body_html = f"""
                <html>
                    <head></head>
                    <body>
                      <p><strong>Product name: {product}</strong></p>
                      <p><strong>Updated by user: {product.updated_by}</strong></p>
        
                      <ul>
                        <li><strong>ID:</strong> {product.id}</li>
                        <li><strong>Sku:</strong> {product.sku}</li>
                        <li><strong>Name:</strong> {product.name}</li>
                        <li><strong>Price:</strong> {product.price}</li>
                        <li><strong>Brand:</strong> {product.brand}</li>
                      </ul>
        
                      <p>You can see the product in the following link:
                        <a href='{product.url}'>{product}</a>
                      </p>
                    </body>
                    </html>
                                """
                # get AWS credentials from .env file
                aws_config = dotenv_values(".env")

                # create a new SES resource and specify a region.
                client = boto3.client(
                    'ses',
                    region_name=aws_region,
                    aws_access_key_id=aws_config['AWS_ACCESS_KEY_ID'],
                    aws_secret_access_key=aws_config['AWS_SECRET_ACCESS_KEY'],
                )

                # Try to send the email.

                response = client.send_email(
                    Destination={
                        'ToAddresses': recipients,
                    },
                    Message={
                        'Body': {
                            'Html': {
                                'Charset': charset,
                                'Data': body_html,
                            }
                        },
                        'Subject': {
                            'Charset': charset,
                            'Data': subject,
                        },
                    },
                    Source=sender,
                )
        # display an error if something goes wrong.
        except Exception as e:
            return {
                'success': False,
                'message': e
            }
        except ClientError as e:
            return {
                'success': False,
                'message': e.response['Error']['Message']
            }
        else:
            return {
                'success': True,
                'message': response
            }
