from django.db import models


class Brand(models.Model):
    name = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    sku = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=80, unique=True)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    brand = models.ForeignKey(Brand, on_delete=models.PROTECT, related_name='products')

    def __str__(self):
        return self.name


class ProductCounter(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    counter = models.PositiveIntegerField(default=0)
