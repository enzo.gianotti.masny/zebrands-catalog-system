![Zebrands Logo](https://camo.githubusercontent.com/52bc681b7e119931452a22ce52df307d6c58335a1e29d55a5f882c64bab16241/68747470733a2f2f7a656272616e64732e6d782f77702d636f6e74656e742f75706c6f6164732f323032312f30372f5745422d5a45422d30352d312d31303234783239312e706e67)

# Zebrands - Catalog System

Zebrands - Catalog System is a basic API REST that maintains and stores product information for an e-commerce business.

## Development environment

- Python 3.8
- Django Rest Framework
- SQLite3
- Docker
- Coding Style: PEP8 (auto-formatting with PyCharm)

## Installation with docker

### Clone repository
```
git clone https://gitlab.com/enzo.gianotti.masny/zebrands-catalog-system/
cd zebrands-catalog-system/
```

### AWS SES Configuration  
add .env file with AWS Credentials to send notification emails.

.env structure:  
AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY_ID  
AWS_SECRET_ACCESS_KEY=YOUR_SECRET_ACCESS_KEY  


### Run docker compose
```
docker-compose up --build -d
```

Open your browser at http://localhost:8000


## Testing

Testing runs automatically in Dockerfile


## Demo

This demo was deployed on AWS EC2.

Link: http://ec2-18-228-119-118.sa-east-1.compute.amazonaws.com:8000/

**Note:**
This API uses AWS SES in mode sandbox to send notification emails, only verified emails can receive notifications.
Please contact to enzo.damian.gianotti@gmail.com to register your email on AWS SES.


### Admin User
user: admin  
password: admin

## API doc UI

You can see Open API specification in the following endpoints:

### Swagger UI 
/api/schema/swagger-ui/

### Redoc UI
/api/schema/redoc/


## Project

* [Tasks](https://gitlab.com/enzo.gianotti.masny/zebrands-catalog-system/-/issues)  
* [Kanban](https://gitlab.com/enzo.gianotti.masny/zebrands-catalog-system/-/boards)  
* [Planification](https://gitlab.com/enzo.gianotti.masny/zebrands-catalog-system/-/milestones)  
* [Realeases](https://gitlab.com/enzo.gianotti.masny/zebrands-catalog-system/-/releases)  
* [Changelog](https://gitlab.com/enzo.gianotti.masny/zebrands-catalog-system/-/blob/main/CHANGELOG.md)

