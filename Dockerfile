FROM python:3.8

RUN apt-get update

COPY requirements.txt requirements.txt
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

WORKDIR /app
COPY . /app

RUN ["python3", "manage.py", "test"]