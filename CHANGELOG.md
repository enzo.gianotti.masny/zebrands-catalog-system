# Changelog

## 01-23-2023 | Catalog system v0.1

### Features

* adds open api documention from code
* creates some unit test for brand and notifications


## 01-23-2023 | First version | Catalog system v0.0

### Features

* allows admin users to manage brands, products, and users
* anonymous users can view products
* notifies all admins of a change in a product via AWS SES
* tracks the number of times each product is queried by anonymous users

